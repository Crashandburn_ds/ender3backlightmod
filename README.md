# Ender 3 Backlight Mod

Tired of the constantly on LCD backlight, I created this mod to control the brightness of the backlight. Using interrupts driven by the rotary encoder the screen stays on for 10 seconds before fading down. When the rotary encoder is turned the screen ramps up to full brightness again.
This mod allows for an external arduino to control the brightness of the lcd
EXTERNAL HARDWARE IS REQUIRED.
# New Features
  - Variables for Maximum and Minimum brightness
  - Variable for time screen remains on
  - Variable fade up and fade down times

### Requirements
  - Arduino Pro Mini (or 328p Compatible dev board)
  - Arduino IDE
  - FTDI Cable

### Installation
  - Download and flash the firmware to the arduino.
  - Wire the rotary encoder to pins 2 & 3.
  - Rotate the 620 ohm resistor to break the connection to the 5v rail (this rail can be used to power the arduino)
  - Wire the backlight to pin 5 though the 620 ohm resistor.
  - Connect the arduino to GND and connect RAW to the 5V rail of the screen