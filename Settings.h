#define ROTARYA 3          ///Pin Connected to the rotary encoder (THIS MUST BE AN INTERRUPT PIN)
#define ROTARYB 2          ///Pin Connected to the rotary encoder (THIS MUST BE AN INTERRUPT PIN)
#define BACKLIGHTPWM 5     ///Pin Connected to the LCD Backlight (MUST BE A PWM PIN)

#define SECSTOFADEDOWN 5   ///How many seconds to fully fade from max to min (Fade Down)
#define SECSTOFADEUP 1     ///How many seconds to fully fade from min to max (Fade Up)
#define SECSTOFADE 10      ///How many seconds the lcd remains on before fading down

#define MAX_BRIGHTNESS 255 ///Sets the maximum brightness of the LCD
#define MIN_BRIGHTNESS 0   ///Sets the minimum brightness of the LCD

