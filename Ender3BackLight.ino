//////////////////////////////////////////
///CONFIGURE ALL SETTINGS IN SETTINGS.H///
//////////////////////////////////////////
 
 #include "Settings.h"

const unsigned long ScreenTimeout = (SECSTOFADE * 1000);
const unsigned long BrightnessFadeUpRate = (SECSTOFADEDOWN * 1000) / 255;
const unsigned long BrightnessFadeDownRate = (SECSTOFADEUP * 1000) / 255;

unsigned long PreviousMillis = 0;

volatile bool ScreenOn = true;
volatile byte Brightness = MIN_BRIGHTNESS;

volatile unsigned long BrightnessUpdateRate = BrightnessFadeUpRate;
volatile unsigned long PreviousBrightnessMillis = 0;

void setup() {
  ///Configure Pins for detection and backlight
  pinMode(ROTARYA, INPUT_PULLUP);
  pinMode(ROTARYB, INPUT_PULLUP);
  pinMode(BACKLIGHTPWM, OUTPUT);

  ///Attach Interrupt to both pins
  attachInterrupt(digitalPinToInterrupt(ROTARYA), UserIntervention, CHANGE);
  attachInterrupt(digitalPinToInterrupt(ROTARYB), UserIntervention, CHANGE);
}

void loop() {
  ///Get Current Millis time
  unsigned long currentMillis = millis();

  ///Fade Down on timeout
  if (currentMillis - PreviousMillis >= ScreenTimeout) {
    ScreenOn = false;
    BrightnessUpdateRate = BrightnessFadeUpRate;
  }

  ///Call Handler
  BrightnessLoop();
}

void BrightnessLoop() {
  ///Get Current Millis time
  unsigned long currentMillis = millis();

  ///Controls the fade up / down time
  if (currentMillis - PreviousBrightnessMillis >= BrightnessUpdateRate) {
    ///Hold Reference to last run time
    PreviousBrightnessMillis = currentMillis;

    ///Set Brightness dependant on screen state
    if (ScreenOn) {
      if (Brightness < MAX_BRIGHTNESS) { ///Ramp up brightness if not at MAX_BRIGHTNESS
        Brightness++;
      }
    } else {
      if (Brightness > MIN_BRIGHTNESS) { ///Ramp down brightness if not at MIN_BRIGHTNESS
        Brightness--;
      }
    }
  }

  ///Write PWM to backlight Pin
  analogWrite(BACKLIGHTPWM, Brightness);
}

void UserIntervention() {
  ///Hold reference to last user interaction
  PreviousMillis = millis();

  ///Set screen state and fade down rate
  ScreenOn = true;
  BrightnessUpdateRate = BrightnessFadeDownRate;
}

